# Homepage

Code for https://hemleb.de.

## Favicon

The favicon was created with the favicon.io tool at https://favicon.io.
The graphic is from the open source project Twemoji at https://twemoji.twitter.com.

# Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run serve
```
