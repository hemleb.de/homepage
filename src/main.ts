import { createApp } from "vue";
import App from "./App.vue";
import "./index.css";
import icons from "./icons";
import router from "./router";

createApp(App).use(router).use(icons).mount("#app");
