import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { App } from "@vue/runtime-core";
import {
  faGitlab,
  faGithub,
  faInstagram,
  faLinkedinIn
} from "@fortawesome/free-brands-svg-icons";
import {
  faEnvelope,
  faLongArrowAltLeft,
  faLongArrowAltRight,
} from "@fortawesome/free-solid-svg-icons";

library.add(faGitlab);
library.add(faGithub);
library.add(faInstagram);
library.add(faLinkedinIn);
library.add(faEnvelope);
library.add(faLongArrowAltLeft);
library.add(faLongArrowAltRight);

export default {
  install: (app: App) => {
    app.component("font-awesome-icon", FontAwesomeIcon);
  },
};
