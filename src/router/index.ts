import {
  createRouter,
  createWebHistory,
  RouteLocationNormalized,
} from "vue-router";
import Home from "../views/Home.vue";
import Sascha from "../views/Sascha.vue";
import Phillip from "../views/Phillip.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/sascha",
    name: "sascha",
    component: Sascha,
    meta: {
      transition: "slide-right",
    },
  },
  {
    path: "/phillip",
    name: "phillip",
    component: Phillip,
    meta: {
      transition: "slide-left",
    },
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

router.afterEach((to, from) => {
  const isRoot = (route: RouteLocationNormalized) => route.path === "/";

  // if this is the first initial page load, we always fade
  // this prevents weird transitions when deeplinking to sub pages with transition meta
  if (!from.name) {
    to.meta.transition = "";
    return;
  }

  // route can define the transition itself. if that's the case we do not need to detect it
  if (to.meta.transition) {
    return;
  }

  // if we come from a route with a 'slide' transition, apply the reversed slide animation
  if (from.meta.transition) {
    switch (from.meta.transition) {
      case "slide-right":
        to.meta.transition = "slide-left";
        return;
      case "slide-left":
        to.meta.transition = "slide-right";
        return;
    }
  }

  const toDepth = isRoot(to) ? 0 : to.path.split("/").length;
  const fromDepth = from.path.split("/").length;

  to.meta.transition = toDepth < fromDepth ? "slide-up" : "slide-down";
});

export default router;
