# syntax=docker/dockerfile:1.3

FROM node:lts as build-frontend

WORKDIR /frontend
COPY package.json yarn.lock ./

RUN --mount=type=cache,target=/usr/local/share/.cache/yarn/v6 \
    set -eux; \
    yarn;

COPY . .

RUN set -eux; \
    yarn run build

FROM golang:1.17 as build-server

WORKDIR /build

ENV GOPATH=/go

COPY go.* ./

RUN --mount=type=cache,target=/go \
    go mod download

COPY server.go ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /server server.go

FROM scratch

WORKDIR /app

COPY --from=build-server /server ./
COPY --from=build-frontend /frontend/dist ./static

EXPOSE 5000
EXPOSE 5001
ENTRYPOINT [ "/app/server" ]