package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	metrics "github.com/slok/go-http-metrics/metrics/prometheus"
	"github.com/slok/go-http-metrics/middleware"
	"github.com/slok/go-http-metrics/middleware/std"
	zaplogfmt "github.com/sykesm/zap-logfmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type spaHandler struct {
	staticPath string
	indexPath  string
	fileserver http.Handler
}

type logRecord struct {
	http.ResponseWriter
	status int
}

func (r *logRecord) Header() http.Header {
	return r.ResponseWriter.Header()
}

func (r *logRecord) Write(p []byte) (int, error) {
	return r.ResponseWriter.Write(p)
}

func (r *logRecord) WriteHeader(status int) {
	r.status = status
	r.ResponseWriter.WriteHeader(status)
}

func newSpaHandler(staticPath string, indexPath string) spaHandler {
	return spaHandler{
		staticPath: staticPath,
		indexPath:  indexPath,
		fileserver: http.FileServer(http.Dir(staticPath)),
	}
}

func (h spaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	path = filepath.Join(h.staticPath, path)

	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		http.ServeFile(w, r, filepath.Join(h.staticPath, h.indexPath))
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	h.fileserver.ServeHTTP(w, r)
}

func main() {
	logger := createLogger()
	loggerMw := createLoggerMiddlewareFactory(logger)

	go func() {
		logger.Infow("started ops routes", "port", 5001)
		logger.Fatal(http.ListenAndServe(":5001", loggerMw(createOpsHandler())))
	}()

	go func() {
		logger.Infow("started app routes", "port", 5000)
		logger.Fatal(http.ListenAndServe(":5000", loggerMw(createAppHandler())))
	}()

	// Wait until some signal is captured.
	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, syscall.SIGTERM, syscall.SIGINT)
	<-sigC
}

func createLogger() *zap.SugaredLogger {
	config := zap.NewProductionEncoderConfig()
	logger := zap.New(zapcore.NewCore(
		zaplogfmt.NewEncoder(config),
		os.Stdout,
		zapcore.DebugLevel,
	))

	return logger.Sugar()
}

func createLoggerMiddlewareFactory(logger *zap.SugaredLogger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			record := &logRecord{
				ResponseWriter: rw,
				status:         200,
			}
			startTime := time.Now()
			next.ServeHTTP(record, r)
			duration := time.Now().Sub(startTime)

			log := logger.With(
				"host", r.Host,
				"method", r.Method,
				"referer", r.Referer(),
				"request_id", r.Header.Get("X-Request-Id"),
				"status_code", record.status,
				"url", r.URL.Path,
				"useragent", r.UserAgent(),
				"version", fmt.Sprintf("%d.%d", r.ProtoMajor, r.ProtoMinor),
				"duration", fmt.Sprint(duration),
			)

			if record.status >= 500 {
				log.Error("server error")
			} else if record.status >= 400 {
				log.Warn("invalid request")
			} else {
				log.Info("request ok")
			}
		})
	}
}

func createAppHandler() http.Handler {
	mux := http.NewServeMux()
	mdlw := middleware.New(middleware.Config{
		Recorder: metrics.NewRecorder(metrics.Config{}),
	})

	mux.Handle("/", newSpaHandler("./static", "index.html"))

	return std.Handler("", mdlw, mux)
}

func createOpsHandler() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/healthz", func(rw http.ResponseWriter, r *http.Request) {
		rw.WriteHeader(http.StatusOK)
		io.WriteString(rw, "ok\n")
		return
	})

	mux.Handle("/metrics", promhttp.Handler())

	return mux
}
